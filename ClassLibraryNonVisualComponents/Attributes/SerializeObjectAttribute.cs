﻿using System;

namespace ClassLibraryNonVisualComponents.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class SerializeObjectAttribute : Attribute
    {
    }
}
