﻿namespace ClassLibraryNonVisualComponents.Enums
{
    public enum FileType
    {
        JSON,
        XML
    }
}
