﻿using ClassLibraryNonVisualComponents.Attributes;
using ClassLibraryNonVisualComponents.Enums;
using ClassLibraryNonVisualComponents.Serializers;
using ClassLibraryNonVisualComponents.Serializers.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;

namespace ClassLibraryNonVisualComponents
{
    public partial class RestoreComponent : Component
    {
        private readonly Dictionary<FileType, string> fileExtensions = new Dictionary<FileType, string>
        {
            { FileType.JSON, ".json" }
        };
        private readonly Dictionary<FileType, IDeserializer> serializers = new Dictionary<FileType, IDeserializer>
        {
            { FileType.JSON, new JsonDeserializer() }
        };

        public FileType FileType { get; set; }
        public string FileName { get; set; }

        public RestoreComponent()
        {
            InitializeComponent();
        }

        public RestoreComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public T GetData<T>(string zipFilePath)
        {
            if (typeof(T).GetCustomAttributes(typeof(SerializeObjectAttribute), false).Length == 0)
                throw new Exception("Class is not supported");

            if (!File.Exists(zipFilePath))
                throw new Exception("File is not exist");

            if (!fileExtensions.ContainsKey(FileType))
                throw new Exception("Extension is not supported");

            var archive = ZipFile.OpenRead(zipFilePath);
            var entry = archive.GetEntry(FileName + fileExtensions[FileType]);
            if (entry == null)
                throw new Exception("File is not exist");

            var file = new StreamReader(entry.Open()).ReadToEnd();
            try
            {
                return serializers[FileType].Deserialize<T>(file);
            }
            catch (Exception e)
            {
                throw new Exception("File does not contains this type of object", e);
            }
        }
    }
}
