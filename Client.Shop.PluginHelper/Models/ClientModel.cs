﻿using System.Collections.Generic;

namespace Client.Shop.PluginHelper.Models
{
    public class ClientModel
    {
        public string FIO { get; set; }
        public string EMail { get; set; }
        public string Address { get; set; }
        public int ClientStatus { get; set; }
        public int PurchaseAmount { get; set; }
        public List<ProductModel> ProductsSubs { get; set; }
        public List<ProductModel> ProductsBuy { get; set; }

        public ClientModel()
        {
            ProductsSubs = new List<ProductModel>();
            ProductsBuy = new List<ProductModel>();
        }
    }
}
