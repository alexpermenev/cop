﻿using System.Linq;
using System.Windows.Forms;

namespace ClassLibraryTreeView
{
    public partial class TreeViewOwn: UserControl
    {
        private string[] propertyNames;

        public TreeViewOwn()
        {
            InitializeComponent();
        }

        public void Configure(string[] propertyNames)
        {
            this.propertyNames = propertyNames;
        }

        public void AddInstance(object obj)
        {
            TreeNode current = null;
            for (int i = 0; i < propertyNames.Length; i++)
            {
                current = AddNode(obj.GetType().GetProperty(propertyNames[i]).GetValue(obj).ToString(), current?.Nodes ?? treeView1.Nodes);
            }
        }

        private TreeNode AddNode(string value, TreeNodeCollection current)
        {
            return current.Find(value, false).FirstOrDefault() ?? current.Add(value, value);
        }
    }
}
