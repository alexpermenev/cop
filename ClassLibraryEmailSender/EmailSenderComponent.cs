﻿using ClassLibraryEmailSender.Interfaces;
using System.ComponentModel;

namespace ClassLibraryNonVisualComponents
{
    public partial class EmailSenderComponent : Component
    {
        public EmailSenderComponent()
        {
            InitializeComponent();
        }

        public EmailSenderComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void SendEmail(ISender sender, string message)
        {
            sender.SendMail(message);
        }
    }
}
