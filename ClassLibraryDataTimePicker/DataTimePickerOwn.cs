﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace ClassLibraryDataTimePicker
{
    public partial class DataTimePickerOwn : UserControl
    {
        public DateTime Value
        {
            get
            {
                if (!_isCorrect)
                    throw new Exception("Значение неверное");
                return _value;
            }
        }

        [Category("Спецификация"), Description("Нижняя граница даты")]
        public DateTime? FromDate { get; set; }
        [Category("Спецификация"), Description("Верхняя граница даты")]
        public DateTime? ToDate { get; set; }

        private DateTime _value;
        private bool _isCorrect;
        private bool _isEmptyDate;

        public DataTimePickerOwn()
        {
            InitializeComponent();
        }

        public void CheckDate(object sender, EventArgs e)
        {
            _isEmptyDate = (
                FromDate.HasValue && ToDate.HasValue
            );
            _isCorrect = (
                _isEmptyDate &&
                dateTimePicker.Value.Date >= FromDate?.Date &&
                dateTimePicker.Value.Date <= ToDate?.Date
            );
            if (!_isCorrect)
            {
                if (_isEmptyDate)
                {
                    label.Text = "Не введены ограничители";
                }
                else
                {
                    label.Text = "Некорректная дата";
                }
            }
            else
            {
                label.Text = "";
            }
            _value = dateTimePicker.Value;
        }

        private void DataTimePickerOwn_Load(object sender, EventArgs e)
        {
            CheckDate(null, null);
            dateTimePicker.ValueChanged += CheckDate;
        }
    }
}
