﻿using ClassLibraryDeliverySender.Abstract;
using System;
using System.ComponentModel;

namespace ClassLibraryNonVisualComponents
{
    public partial class DeliverySenderComponent : Component
    {
        public DeliverySenderComponent()
        {
            InitializeComponent();
        }

        public DeliverySenderComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public Action<TModel> SendDeliverGenerate<TModel>(DeliveryCreator<TModel> deliveryCreator)
        {
            return (TModel model) =>
            {
                deliveryCreator.SendDeliver(model);
            };
        }
    }
}
