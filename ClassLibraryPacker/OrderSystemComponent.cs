﻿using ClassLibraryPacker.Interfaces;
using System;
using System.ComponentModel;

namespace ClassLibraryPacker
{
    public partial class OrderSystemComponent : Component
    {
        public OrderSystemComponent()
        {
            InitializeComponent();
        }

        public OrderSystemComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public Action<ISendHandler<T>, T> GenerateOrderSystem<T>()
        {
            return (packer, model) => packer.Send(model);
        }
    }
}
